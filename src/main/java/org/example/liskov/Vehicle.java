package org.example.liskov;

import java.util.ArrayList;
import java.util.List;

public class Vehicle {
    public void startEngine(){
    }
    public void startMotion(){}

}

class Car extends Vehicle{
    @Override
    public void startEngine(){
    }
    @Override
    public void startMotion(){}
}

class Bicycle extends Vehicle{
    @Override
    public void startEngine(){
        throw new RuntimeException();
    }
    @Override
    public void startMotion(){}
}

class VehicleMonitor{
    public void startVehicle(){
        List<Vehicle> vehicles = new ArrayList<Vehicle>();
        Vehicle car = new Car();
        Vehicle cycle = new Bicycle();

        vehicles.add(car);
        vehicles.add(cycle);

        vehicles.forEach(vehicle-> vehicle.startEngine());
    }
}





