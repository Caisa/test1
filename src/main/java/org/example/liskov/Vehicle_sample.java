package org.example.liskov;

public class Vehicle_sample {
    public void startEngine_sample(){
        }

}
class Car_sample extends  Vehicle_sample{
    @Override
    public void startEngine_sample() {}

}
class Bicycle_sample extends Vehicle_sample{
    @Override
    public void startEngine_sample(){
        throw new RuntimeException();
    }

}
