package org.example.openclose;

public interface Operation{
    public double calculate (double var1, double var2);
}
 class AddOperation implements Operation{

    @Override
     public double calculate(double var1, double var2){
        return var1 + var2;
    }

}
